from .statistical_tests import test_kolmogorov_smirnov

__all__ = ["test_kolmogorov_smirnov"]
