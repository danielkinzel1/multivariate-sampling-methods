from scipy.stats import ks_2samp
import numpy as np


def test_kolmogorov_smirnov(split_a: list | np.ndarray, split_b: list | np.ndarray):
    """
    Basic Implementation of Kolmogorov Smirnov Test. Works on the mean values of the multivariate data

    :param split_a: Numpy array of multivariate subsample a
    :param split_b: Numpy array of multivariate subsample b
    :return: KstestResult with test statistic value and p-value
    """

    # Calculate mean of each multivariate data point
    split_a_means = [x.sum() / len(x) for x in split_a]
    split_b_means = [x.sum() / len(x) for x in split_b]

    # Calculate Kolmogorov Smirnov Test statistic and p-value
    _result = ks_2samp(data1=split_a_means, data2=split_b_means, alternative="two-sided", method="auto")

    return _result
