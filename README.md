## Subsampling from multivariate Data sets

This repository has been born due to an interesting problem a former study friend told me and is driven by inner 'how hard can it be' and 'what happens if i trie this weird approach' attitude.

### Problem Formulation

Given a multivariate data set *X* we want to create two subsamples *X<sub>1</sub>*, *X<sub>2</sub>* that should both represent the empirical distribution of *X* as good as possible.
We vary this scenario at the beginning to create same size subsamples and add the possibility for drawing a smaller subsample later on.

### Benchmark

As Benchmark I use the Sklearn Function train_test_split. To compare these subsamples to those created by the novel aproaches we conduct a Kolmogorov Smirnov test on the mean values of the data.
By that we investigate how 'probable' it is that two subsamples are drawn from the same distribution . Please remark that this does not represent a valid statistical proof, but it gives us a hint how good the splits represent the original data set.
Additionally we look at 2D density plots for bivariate examples.

### Presented Approaches

1. Comparison Based Sampling (half half case)
   - Idea: every empirical distrubtion is definies by its components. To get similar empirical distributions, we need to similar component sets
   - Randomly draw one element from the original data, add it to subsample a
   - find most similar element in the data, add it to subsample b
   - proceed until all data points are either in a or b
   - Runs in O(N<sup>3</sup>), better suited for small sample sizes
   - Visual comparison of 2d Histograms for bivariate Case (see demo notebook for more):
   ![image](docs/Bivariate_example_ComparisonBased.png)
2. Clustering Based Sampling (with desired sample size)
   - Idea: If we manage to draw random samples from each decentile (or other partition of our distribution) we might end up with two similar data sets
   - Cluster Data to similar clusters
   - Draw split Sample (desired size) from each cluster and aggregate it
   - Combine splitted samples and rest to 2 sets
   - Runs in O(N<sup>2</sup>), dominated by KMeans complexity
   - Visual comparison of 2d Histograms for bivariate Case (see demo_clusterbased_subsampling.ipynb for more details):
   ![image_cluster](docs/Bivariate_example_Clusterbased.png)

This list will be expanded the more ideas I have :)

### Project Structure

The sampling algorithms and helper functions are implemented in intelligent_sampling module

Demonstrations of them as well as the statistical and visual investigation are presented in Jupyter Notebooks in the demo_notebooks directory


