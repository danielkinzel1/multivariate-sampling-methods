import random

import numpy as np
import random as rnd
import sys

from sklearn.cluster import KMeans


def _calc_distance_euclidian(elem_a: np.ndarray, elem_b: np.ndarray) -> float:
    """
    Calculate euclidian distance between two n-dimensional data points.

    :param elem_a: first element
    :param elem_b: second element
    :return:
    """
    return pow((elem_a - elem_b), 2).sum()


def _find_most_sim(values: np.ndarray, elem: np.ndarray) -> int:
    """
    Find most similar element in array of values. Works for n-dimensional elements based on euclidian distance.

    :param values: Array of n-dimensional values
    :param elem: value to find most similar element for
    :return: index of most similar element in values
    """
    _best_sim = sys.maxsize
    _best_idx = -1

    for j in range(len(values)):
        _sim = _calc_distance_euclidian(elem_a=elem, elem_b=values[j])
        if _sim < _best_sim:
            _best_sim = _sim
            _best_idx = j

    return _best_idx


def subsample_simbased_equal(data: np.ndarray) -> (list, list):
    """
    Create two subsample out of a numerical given 2d numpy array with same size. Subsampling is performed via randomly drawing
    an element of data and looking for the most similar corresponding one. By that two similar subsamples are created.

    :param data: 2d numpy array

    :return: tuple of two 2d numpy arrays
    """

    subsample_a = []
    subsample_b = []

    half = int(len(data) / 2)

    i = 0
    while i < half:
        # choose random element from values
        idx_random = rnd.randint(0, len(data) - 1)
        value_random = data[idx_random]
        data = np.delete(arr=data, obj=idx_random, axis=0)

        # find most similar element
        idx_most_sim = _find_most_sim(values=data, elem=value_random)
        value_most_sim = data[idx_most_sim]
        data = np.delete(arr=data, obj=idx_most_sim, axis=0)

        # put chosen in one and most similar in other list
        subsample_a.append(value_random)
        subsample_b.append(value_most_sim)

        i += 1

    # Deal with case the data length is uneven
    if len(data) != 0:
        subsample_a.append(data[0])

    return subsample_a, subsample_b


def subsample_clusterbased(data: np.ndarray, test_size: float, n_cluster=10, max_iter=300) -> (np.ndarray, np.ndarray):
    """
    Create two subsamples out of a numerical given data 2d numpy array.

    :param max_iter: number of maximum iterations for KMeans Clustering
    :param data: 2d numpy array
    :param test_size: fraction ob subsample two create
    :param n_cluster: number ob clusters for the clustering algorithm

    :return: tuple of two created subsamples
    """

    if len(data) == 0:
        raise ValueError(f"data parameter was array without elements {data}")

    # Deal with tiny samples
    if n_cluster > len(data): n_cluster = len(data) - 1

    # Construct Clusters and sizes
    cluster_outcome = KMeans(n_clusters=n_cluster, max_iter=max_iter).fit(data)
    list_clusters = []
    size_clusters = []
    for n in range(n_cluster):
        _cluster = data[np.where(cluster_outcome.labels_ == n)]
        list_clusters.append(_cluster)
        size_clusters.append(len(_cluster))

    # Construct subsample sizes
    size_subsamples = []
    h = 0
    for s in size_clusters:
        _s = round(s * test_size)
        if _s == 0:
            _s = 1
        assert(_s <= len(list_clusters[h]))
        size_subsamples.append(_s)
        h += 1

    # Construct Subsamples
    sample_a = []
    sample_b = []
    for i in range(n_cluster):
        _rand_indizes = random.sample(range(0, len(list_clusters[i])), size_subsamples[i])
        sample_a.append(list_clusters[i][_rand_indizes])
        _rest = np.delete(arr=list_clusters[i], obj=_rand_indizes, axis=0)
        if len(_rest) > 0:
            sample_b.append(_rest)

    sample_a = np.concatenate(sample_a, axis=0)
    sample_b = np.concatenate(sample_b, axis=0)

    return sample_b, sample_a
